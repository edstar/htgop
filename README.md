# High-Tune: Gas Optical Properties

This library loads the optical properties of a gas.

## How to build

This program relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build.
It also depends on the
[RSys](https://gitlab.com/vaplv/rsys/),
library.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Release notes

### Version 0.1.2

Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8 has become
obsolete.

### Version 0.1.1

Remove the hard-coded boundaries of the shortwave/longwave domains. Actually
"shortwave" and "longwave" are only that define that the source of radiation is
whether external or internal to the medium, respectively.

### Version 0.1

- Add the `htgop_get_<lw|sw>_spectral_intervals` functions: they return the
  indices of the lower and upper spectral intervals that include a given range
  of long/short waves.
- Add the `htgop_find_<lw|sw>_spectral_interval_id` functions: they return the
  index of the spectral interval that includes the submitted short/long wave.
- Remove the functions explicitly relying onto the CIE 1931 XYZ color space,
  i.e. `htgop_sample_sw_spectral_interval_CIE_1931_<X|Y|Z>` and
  `htgop_get_sw_spectral_intervals_CIE_XYZ`.

### Version 0.0.2

- Fix an issue when the parsed line is greater than 128 characters.

## Licenses

Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com). htgop is
free software released under the GPL v3+ license: GNU GPL version 3 or later.
You are welcome to redistribute it under certain conditions; refer to the
COPYING file for details.

