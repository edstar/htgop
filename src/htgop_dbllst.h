/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_DBLLST_H
#define HTGOP_DBLLST_H

#include <rsys/dynamic_array_double.h>

/* Generate the darray_dbllst dynamic array */
#define DARRAY_NAME dbllst
#define DARRAY_DATA struct darray_double
#define DARRAY_FUNCTOR_INIT darray_double_init
#define DARRAY_FUNCTOR_RELEASE darray_double_release
#define DARRAY_FUNCTOR_COPY darray_double_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_double_copy_and_release
#include <rsys/dynamic_array.h>

#endif /* HTGOP_DBLLST_H */

