/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_C_H
#define HTGOP_C_H

#include "htgop_layer.h"
#include "htgop_spectral_intervals.h"

#include <rsys/dynamic_array.h>
#include <rsys/ref_count.h>

/* Helper macros to convert from wave number to wavelength and vice versa */
#define WNUM_TO_WLEN(Num) (1.0e7 / ((double)(Num)))
#define WLEN_TO_WNUM(Len) WNUM_TO_WLEN(Len)

/* Generate the dynamic array of level */
#define DARRAY_NAME level
#define DARRAY_DATA struct htgop_level
#include <rsys/dynamic_array.h>

struct htgop {
  struct htgop_ground ground;

  /* Description of the spectral intervals for the short/long wave domain */
  struct spectral_intervals lw_specints;
  struct spectral_intervals sw_specints;

  /* Cumulative distribution function for the Short Wave CIE XYZ values */
  struct darray_double sw_X_cdf;
  struct darray_double sw_Y_cdf;
  struct darray_double sw_Z_cdf;

  struct darray_layer layers; /* Par layer data */
  struct darray_level levels; /* Per level data (#level = #layer + 1 ) */

  int verbose; /* Verbosity level */
  struct logger* logger;
  struct mem_allocator* allocator;
  ref_T ref;
};

static INLINE int
cmp_dbl(const void* a, const void* b)
{
  const double d0 = *((const double*)a);
  const double d1 = *((const double*)b);
  return d0 < d1 ? -1 : (d0 > d1 ? 1 : 0);
}

extern LOCAL_SYM void
log_err
  (const struct htgop* htgop,
   const char* fmt,
  ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
  ;

extern LOCAL_SYM void
log_warn
  (const struct htgop* htgop,
   const char* fmt,
  ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
  ;

extern LOCAL_SYM double
layer_lw_spectral_interval_tab_interpolate_ka
  (const struct htgop_layer_lw_spectral_interval_tab* tab,
   const double x_h2o,
   const double* x_h2o_upp); /* Pointer toward the 1st tabbed xH2O >= x_h2o */

extern LOCAL_SYM double
layer_sw_spectral_interval_tab_interpolate_ka
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o,
   const double* x_h2o_upp); /* Pointer toward the 1st tabbed xH2O >= x_h2o */

extern LOCAL_SYM double
layer_sw_spectral_interval_tab_interpolate_ks
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o,
   const double* x_h2o_upp); /* Pointer toward the 1st tabbed xH2O >= x_h2o */

extern LOCAL_SYM double
layer_sw_spectral_interval_tab_interpolate_kext
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o,
   const double* x_h2o_upp); /* Pointer toward the 1st tabbed xH2O >= x_h2o */

/* Return the *inclusive* lower/upper index of the spectral intervals that
 * overlaps the submitted wave number range */
extern LOCAL_SYM res_T
get_spectral_intervals
  (const struct htgop* htgop,
   const char* func_name,
   const double wnum_range[2],
   const double* wnums,
   const size_t nwnums,
   size_t specint_range[2]);

#endif /* HTGOP_C_H */

