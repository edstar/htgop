/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_READER_H
#define HTGOP_READER_H

#include <rsys/rsys.h>
#include <rsys/stretchy_array.h>
#include <string.h>

struct reader {
  FILE* stream;
  const char* name;
  size_t iline;
  char* line;
};

static INLINE void
reader_init(struct reader* rdr, FILE* stream, const char* name)
{
  ASSERT(rdr && stream && name);
  memset(rdr, 0, sizeof(struct reader));
  rdr->stream = stream;
  rdr->name = name;
  rdr->iline = 0;
  rdr->line = sa_add(rdr->line, 128);
  ASSERT(rdr->line);
}

static INLINE void
reader_release(struct reader* rdr)
{
  ASSERT(rdr);
  sa_release(rdr->line);
}

/* Read a non empty line */
static INLINE char*
read_line(struct reader* rdr)
{
  const int chunk = 32;

  do {
    if(!fgets(rdr->line, (int)sa_size(rdr->line), rdr->stream)) return NULL;
    ++rdr->iline;

    /* Ensure that the whole line is read */
    while(!strrchr(rdr->line, '\n') && !feof(rdr->stream)) {
      char* more = sa_add(rdr->line, (size_t)chunk);
      CHK(fgets(more-1/*prev null char*/, chunk+1/*prev null char*/, rdr->stream));
    }

    rdr->line[strcspn(rdr->line, "#\n\r")] = '\0'; /* Rm new line & comments */
  } while(strspn(rdr->line, " \t") == strlen(rdr->line)); /* Empty line */
  return rdr->line;
}

#endif /* HTGOP_READER_H */

