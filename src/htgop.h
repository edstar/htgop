/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_H
#define HTGOP_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(HTGOP_SHARED_BUILD) /* Build shared library */
  #define HTGOP_API extern EXPORT_SYM
#elif defined(HTGOP_STATIC) /* Use/build static library */
  #define HTGOP_API extern LOCAL_SYM
#else /* Use shared library */
  #define HTGOP_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the htgop function `Func'
 * returns an error. One should use this macro on htgop function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define HTGOP(Func) ASSERT(CONCAT(htgop_, Func) == RES_OK)
#else
  #define HTGOP(Func) CONCAT(htgop_, Func)
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct htgop;

struct htgop_ground {
  double temperature; /* In Kelvin */
  double lw_emissivity; /* Long wave emissivity */
  double sw_emissivity; /* Short wave emissivity */
};

struct htgop_spectral_interval {
  double wave_numbers[2]; /* Lower/Upper wave number of the interval in cm^-1 */
  const double* quadrature_pdf; /* Normalized PDF of the quadrature */
  const double* quadrature_cdf; /* CDF of the quadrature */
  size_t quadrature_length; /* #quadrature points of the interval */
  const struct htgop* htgop;
};

struct htgop_level {
  double pressure; /* In Pascal */
  double temperature; /* In Kelvin */
  double height; /* In meter */
};

struct htgop_layer {
  double x_h2o_nominal; /* Nominal molar fraction of water vapor */
  const double* x_h2o_tab; /* Tabulated xH2O */
  size_t tab_length; /* Length of the tabulated xH2O */
  size_t lw_spectral_intervals_count;
  size_t sw_spectral_intervals_count;
  const struct htgop* htgop;
  const void* data__; /* Internal data */
};

struct htgop_layer_lw_spectral_interval {
  const double* ka_nominal; /* Per quadrature point absorption coef */
  size_t quadrature_length;
  const struct htgop* htgop;
  /* Internal data */
  const void* data__;
  const void* layer__;
};

struct htgop_layer_lw_spectral_interval_tab {
  const double* x_h2o_tab; /* <=> layer.x_h2o_tab */
  const double* ka_tab; /* Tabulated absorption coef */
  size_t tab_length; /* == lengh of the tabulated xH2O */
  const struct htgop* htgop;
};

struct htgop_layer_sw_spectral_interval {
  const double* ka_nominal; /* Per quadrature point absorption coef */
  const double* ks_nominal; /* Per quadrature point scattering coef */
  size_t quadrature_length;
  const struct htgop* htgop;
  /* Internal data */
  const void* data__;
  const void* layer__;
};

struct htgop_layer_sw_spectral_interval_tab {
  const double* x_h2o_tab; /* <=> layer.x_h2o_tab */
  const double* ka_tab; /* Tabulated absorption coef */
  const double* ks_tab; /* Tabulated scattering coef */
  size_t tab_length; /* == lengh of the tabulated xH2O */
  const struct htgop* htgop;
};

BEGIN_DECLS /* HTGOP API */

/*******************************************************************************
 * HTGOP device management
 ******************************************************************************/
HTGOP_API  res_T
htgop_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct htgop** htgop);

HTGOP_API res_T
htgop_ref_get
  (struct htgop* htgop);

HTGOP_API res_T
htgop_ref_put
  (struct htgop* htgop);

/*******************************************************************************
 * Loading functions
 ******************************************************************************/
HTGOP_API res_T
htgop_load
  (struct htgop* htgop,
   const char* filename);

HTGOP_API res_T
htgop_load_stream
  (struct htgop* htgop,
   FILE* stream);

/*******************************************************************************
 * Getters of the the loaded data
 ******************************************************************************/
HTGOP_API res_T
htgop_get_ground
  (const struct htgop* htgop,
   struct htgop_ground* ground);

HTGOP_API res_T
htgop_get_layers_count
  (const struct htgop* htgop,
   size_t* nlayers);

HTGOP_API res_T
htgop_get_levels_count
  (const struct htgop* htgop,
   size_t* nlevels);

HTGOP_API res_T
htgop_get_level
  (const struct htgop* htgop,
   const size_t ilevel,
   struct htgop_level* level);

HTGOP_API res_T
htgop_get_layer
  (const struct htgop* htgop,
   const size_t ilayer,
   struct htgop_layer* layer);

HTGOP_API res_T
htgop_get_lw_spectral_intervals_count
  (const struct htgop* htgop,
   size_t* nspecints);

HTGOP_API res_T
htgop_get_sw_spectral_intervals_count
  (const struct htgop* htgop,
   size_t* nspecints);

HTGOP_API res_T
htgop_get_lw_spectral_intervals_wave_numbers
  (const struct htgop* htgop,
   const double* wave_numbers[]);

HTGOP_API res_T
htgop_get_sw_spectral_intervals_wave_numbers
  (const struct htgop* htgop,
   const double* wave_numbers[]);

HTGOP_API res_T
htgop_get_lw_spectral_interval
  (const struct htgop* htgop,
   const size_t ispectral_interval,
   struct htgop_spectral_interval* interval);

HTGOP_API res_T
htgop_get_sw_spectral_interval
  (const struct htgop* htgop,
   const size_t ispectral_interval,
   struct htgop_spectral_interval* interval);

/* Find the id of the long wave interval that includes `wave_number'.  */
HTGOP_API res_T
htgop_find_lw_spectral_interval_id
  (const struct htgop* htgop,
   const double wave_number, /* In cm^-1 */
   size_t* ispectral_interval); /* SIZE_MAX <=> not found */

/* Find the id of the short wave interval that includes `wave_number' */
HTGOP_API res_T
htgop_find_sw_spectral_interval_id
  (const struct htgop* htgop,
   const double wave_number, /* In cm^-1 */
   size_t* ispectral_interval); /* SIZE_MAX <=> not found */

HTGOP_API res_T
htgop_layer_get_lw_spectral_interval
  (const struct htgop_layer* layer,
   const size_t ispectral_interval,
   struct htgop_layer_lw_spectral_interval* spectral_interval);

HTGOP_API res_T
htgop_layer_get_sw_spectral_interval
  (const struct htgop_layer* layer,
   const size_t ispectral_interval,
   struct htgop_layer_sw_spectral_interval* spectral_interval);

HTGOP_API res_T
htgop_layer_lw_spectral_interval_get_tab
  (const struct htgop_layer_lw_spectral_interval* spectral_interval,
   const size_t iquadrature_point,
   struct htgop_layer_lw_spectral_interval_tab* tab);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_get_tab
  (const struct htgop_layer_sw_spectral_interval* spectral_interval,
   const size_t iquadrature_point,
   struct htgop_layer_sw_spectral_interval_tab* tab);

/*******************************************************************************
 * The following functions retrieve the absorption/scattering coefficient in a
 * layer, in short/long wave, at a given spectral interval for a specific
 * quadrature point with respect to a submitted water vapor molar fraction
 * x_h2o. x_h2o is compared to the values of tabulated water vapor molar
 * fractions of the layer: x_h2o in [x1, x2], with the corresponding tabulated
 * values of the required radiative property k(x1) = k1 and k(x2) = k2. Now
 * there is 3 cases:
 *
 * - If x_h2o is smaller than the first tabulated value of the water vapor molar
 *   fraction, x1 and k1 is assumed to be null and k(x_h2o) is computed with a
 *   linear interpolation:
 *      k(x_h2o) = x_h2o * k2/x2
 *
 * - If either k1 or k2 is null, k(x_h2o) is also computed with a simple linear
 *   interpolation:
 *      k(x_h2o) = k1 + (k2-k1)/(x2-x1) * (x_h2o-x1)
 *
 * - Otherwise, k(x_h2o) is computed as bellow:
 *      k(x_h2o) = exp(alpha * ln(x_h2o) + beta)
 *   with:
 *      alpha = (ln(k2) - ln(k1)) / (ln(x2) - ln(x1))
 *      beta  = ln(k1) - alpha * ln(x1)
 ******************************************************************************/
HTGOP_API res_T
htgop_layer_lw_spectral_interval_tab_fetch_ka
  (const struct htgop_layer_lw_spectral_interval_tab* tab,
   const double x_h2o, /* H2O mol / mixture mol */
   double* ka);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_fetch_ka
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o, /* H2O mol / mixture mol */
   double* ka);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_fetch_ks
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o, /* H2O mol / mixture mol */
   double* ks);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_fetch_kext
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o, /* H2O mol / mixture mol */
   double* kext);

/*******************************************************************************
 * Retrieve the boundaries of the radiative properties
 ******************************************************************************/
HTGOP_API res_T
htgop_layer_lw_spectral_interval_quadpoints_get_ka_bounds
  (const struct htgop_layer_lw_spectral_interval* specint,
   const size_t iquad_range[2], /* Range of quadrature points to handle */
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_quadpoints_get_ka_bounds
  (const struct htgop_layer_sw_spectral_interval* specint,
   const size_t iquad_range[2], /* Range of quadrature points to handle */
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_quadpoints_get_ks_bounds
  (const struct htgop_layer_sw_spectral_interval* specint,
   const size_t iquad_range[2], /* Range of quadrature points to handle */
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_quadpoints_get_kext_bounds
  (const struct htgop_layer_sw_spectral_interval* specint,
   const size_t iquad_range[2], /* Range of quadrature points to handle */
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_lw_spectral_interval_tab_get_ka_bounds
  (const struct htgop_layer_lw_spectral_interval_tab* tab,
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_get_ka_bounds
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_get_ks_bounds
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o_range[2],
   double bounds[2]);

HTGOP_API res_T
htgop_layer_sw_spectral_interval_tab_get_kext_bounds
  (const struct htgop_layer_sw_spectral_interval_tab* tab,
   const double x_h2o_range[2],
   double bounds[2]);

/*******************************************************************************
 * Miscellaneous functions
 ******************************************************************************/
/* Return the layer index into which the submitted a 1D position lies */
HTGOP_API res_T
htgop_position_to_layer_id
  (const struct htgop* htgop,
   const double pos,
   size_t* ilayer);

HTGOP_API res_T
htgop_spectral_interval_sample_quadrature
  (const struct htgop_spectral_interval* interval,
   const double r, /* Canonical random number in [0, 1[ */
   size_t* iquad_point); /* Id of the sample quadrature point */

HTGOP_API res_T
htgop_get_sw_spectral_intervals
  (const struct htgop* htgop,
   const double wnum_range[2],
   size_t specint_range[2]);

HTGOP_API res_T
htgop_get_lw_spectral_intervals
  (const struct htgop* htgop,
   const double wnum_range[2],
   size_t specint_range[2]);

END_DECLS

#endif /* HTGOP */

