/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_PARSE_LAYERS_SPECTRAL_INTERVALS_DATA_H
#define HTGOP_PARSE_LAYERS_SPECTRAL_INTERVALS_DATA_H

#include "htgop_c.h"
#include "htgop_reader.h"

#endif /* HTGOP_PARSE_LAYERS_SPECTRAL_INTERVALS_DATA_H */

/*
 * Generate the function that parses a per layer and per spectral interval data
 * for a given domain.
 */

#if !defined(DATA) || !defined(DOMAIN)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

#define XDOMAIN(Name) CONCAT(CONCAT(DOMAIN, _), Name)
#define XDATA(Name) CONCAT(CONCAT(DATA, _), Name)

static res_T
CONCAT(CONCAT(CONCAT(parse_layers_spectral_intervals_,  DATA), _), DOMAIN)
  (struct htgop* htgop, struct reader* rdr)
{
  struct layer* layers = NULL;
  size_t nspecints, nlays, tab_len, quad_len;
  size_t ispecint, ilay, itab, iquad;
  res_T res = RES_OK;
  ASSERT(htgop && rdr);

  layers = darray_layer_data_get(&htgop->layers);
  nlays = darray_layer_size_get(&htgop->layers);
  ASSERT(nlays > 0);

  tab_len = darray_double_size_get(&layers[0].x_h2o_tab);
  ASSERT(tab_len > 0);

  nspecints = darray_double_size_get(&htgop->XDOMAIN(specints).wave_numbers)-1;
  ASSERT(nspecints > 0);

  #define CALL(Func) { if((res = Func) != RES_OK) goto error; } (void)0
  FOR_EACH(ispecint, 0, nspecints) {
    struct CONCAT(layer_, XDOMAIN(spectral_interval))* layspecint;
    struct darray_double* quad; /* Quadrature of the current interval */
    double* XDATA(nominal);
    struct darray_double* XDATA(tab);

    quad = darray_dbllst_data_get
      (&htgop->XDOMAIN(specints).quadrature_pdfs) + ispecint;
    quad_len = darray_double_size_get(quad);

    /* Allocate the per layer data */
    FOR_EACH(ilay, 0, nlays) {
      layspecint = CONCAT(CONCAT(darray_lay_, XDOMAIN(specint)), _data_get)
        (&layers[ilay].XDOMAIN(specints)) + ispecint;
      CALL(darray_double_resize(&layspecint->XDATA(nominal), quad_len));
      CALL(darray_dbllst_resize(&layspecint->XDATA(tab), quad_len));
      FOR_EACH(iquad, 0, quad_len) { /* Tabulated data */
        XDATA(tab) = darray_dbllst_data_get(&layspecint->XDATA(tab)) + iquad;
        CALL(darray_double_resize(XDATA(tab), tab_len));
      }
    }

    FOR_EACH(iquad, 0, quad_len) {
      /* Read per layer nominal data */
      FOR_EACH(ilay, 0, nlays) {
        layspecint = CONCAT(CONCAT(darray_lay_, XDOMAIN(specint)), _data_get)
          (&layers[ilay].XDOMAIN(specints)) + ispecint;
        XDATA(nominal) = darray_double_data_get(&layspecint->XDATA(nominal));
        CALL(cstr_to_double(read_line(rdr), &XDATA(nominal)[iquad]));
      }

      /* Read per layer tabulated data */
      FOR_EACH(itab, 0, tab_len) {
        FOR_EACH(ilay, 0, nlays) {
          layspecint = CONCAT(CONCAT(darray_lay_, XDOMAIN(specint)), _data_get)
            (&layers[ilay].XDOMAIN(specints)) + ispecint;
          XDATA(tab) = darray_dbllst_data_get(&layspecint->XDATA(tab)) + iquad;
          CALL(cstr_to_double(read_line(rdr), &darray_double_data_get(XDATA(tab))[itab]));
          if(darray_double_data_get(XDATA(tab))[itab] < 0) {
            log_err(htgop, "The radiative properties cannot be negative.\n");
            res = RES_BAD_ARG;
            goto error;
          }
        }
      }
    }
  }
  #undef CALL

exit:
  return res;
error:
  goto exit;
}

#undef DATA
#undef DOMAIN
#undef XDOMAIN
#undef XDATA
