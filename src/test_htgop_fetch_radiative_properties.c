/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "test_htgop_utils.h"

#include <rsys/math.h>

/* Generate the check_layer_fetch_lw_ka function */
#define DOMAIN lw
#define DATA ka
#include "test_htgop_fetch_radiative_properties.h"
/* Generate the check_layer_fetch_sw_ka function */
#define DOMAIN sw
#define DATA ka
#include "test_htgop_fetch_radiative_properties.h"
/* Generate the check_layer_fetch_sw_ks function */
#define DOMAIN sw
#define DATA ks
#include "test_htgop_fetch_radiative_properties.h"
/* Generate the check_layer_fetch_sw_ks function */
#define GET_K(Tab, Id) ((Tab)->ka_tab[Id] + (Tab)->ks_tab[Id])
#define DOMAIN sw
#define DATA kext
#include "test_htgop_fetch_radiative_properties.h"

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct htgop* htgop;
  size_t i, n;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s FILENAME\n", argv[0]);
    return 1;
  }

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  CHK(htgop_create(NULL, &allocator, 1, &htgop) == RES_OK);
  CHK(htgop_load(htgop, argv[1]) == RES_OK);

  CHK(htgop_get_layers_count(htgop, &n) == RES_OK);
  CHK(n > 0);

  FOR_EACH(i, 0, n) {
    struct htgop_layer layer;
    CHK(htgop_get_layer(htgop, i, &layer) == RES_OK);
    check_layer_fetch_lw_ka(htgop, &layer);
    check_layer_fetch_sw_ka(htgop, &layer);
    check_layer_fetch_sw_ks(htgop, &layer);
    check_layer_fetch_sw_kext(htgop, &layer);
  }

  CHK(htgop_ref_put(htgop) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
