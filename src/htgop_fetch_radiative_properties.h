/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "htgop_c.h"

#include <rsys/algorithm.h>

#if !defined(DATA) || !defined(DOMAIN)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

/*
 * Generate the functions that fetch and interpolate a radiative property in a
 * given spectral domain
 */

#ifndef GET_DATA
  #define GET_DATA(Tab, Id) ((Tab)->CONCAT(DATA, _tab)[Id])
#endif

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
CONCAT(CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval_tab_fetch_),DATA)
  (const struct CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval_tab)* tab,
   const double x_h2o, /* H2O mol / mixture mol */
   double* out_k)
{
  double* find;

  if(!tab || !out_k) return RES_BAD_ARG;

  find = search_lower_bound(&x_h2o, tab->x_h2o_tab, tab->tab_length,
    sizeof(double), cmp_dbl);
  if(!find) return RES_BAD_ARG;

  *out_k = CONCAT(CONCAT(CONCAT(
    layer_,DOMAIN),_spectral_interval_tab_interpolate_),DATA)(tab, x_h2o, find);
  return RES_OK;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
double
CONCAT(CONCAT(CONCAT(layer_,DOMAIN),_spectral_interval_tab_interpolate_),DATA)
  (const struct CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval_tab)* tab,
   const double x_h2o,
   const double* x_h2o_upp) /* Pointer toward the upper bound used to interpolate */
{
  double x1, x2, k1, k2;
  double k;

  ASSERT(tab && x_h2o_upp);

  ASSERT(x_h2o_upp >= tab->x_h2o_tab);
  ASSERT(x_h2o_upp < tab->x_h2o_tab + tab->tab_length);
  ASSERT(*x_h2o_upp >= x_h2o);
  ASSERT(x_h2o_upp == tab->x_h2o_tab || x_h2o_upp[-1] < x_h2o);

  if(x_h2o_upp == tab->x_h2o_tab) {
    /* The submitted x_h2o is smaller that the first tabulated value of the
     * water vapor molar fraction. Use a simple linear interpolation to define
     * K by assuming that k1 and x1 is null */
    ASSERT(tab->x_h2o_tab[0] >= x_h2o);
    x2 = tab->x_h2o_tab[0];
    k2 = GET_DATA(tab, 0);
    k = k2 / x2*x_h2o;
  } else {
    const size_t i = (size_t)(x_h2o_upp - tab->x_h2o_tab);
    ASSERT(i < tab->tab_length);
    ASSERT(tab->x_h2o_tab[i-0] >= x_h2o);
    ASSERT(tab->x_h2o_tab[i-1] <  x_h2o);
    x1 = tab->x_h2o_tab[i-1];
    x2 = tab->x_h2o_tab[i-0];
    k1 = GET_DATA(tab, i-1);
    k2 = GET_DATA(tab, i-0);

    if(!k1 || !k2) { /* Linear interpolation */
      k = k1 + (k2-k1)/(x2-x1) * (x_h2o-x1);
    } else {
      const double alpha = (log(k2) - log(k1))/(log(x2) - log(x1));
      const double beta = log(k1) - alpha * log(x1);
      k = exp(alpha * log(x_h2o) + beta);
    }
  }
  return k;
}

#undef GET_DATA
#undef DATA
#undef DOMAIN
