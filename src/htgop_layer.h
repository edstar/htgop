/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_LAYER_H
#define HTGOP_LAYER_H

#include "htgop_dbllst.h"
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Long wave spectral interval of a layer. Each data are defined per quadrature
 * point.
 ******************************************************************************/
struct layer_lw_spectral_interval {
  struct darray_double ka_nominal; /* Nominal absorption coef */
  struct darray_dbllst ka_tab; /* Tabulated absorption coef */
};

static INLINE void
layer_lw_spectral_interval_init
  (struct mem_allocator* allocator, struct layer_lw_spectral_interval* inter)
{
  ASSERT(inter);
  darray_double_init(allocator, &inter->ka_nominal);
  darray_dbllst_init(allocator, &inter->ka_tab);
}

static INLINE void
layer_lw_spectral_interval_release(struct layer_lw_spectral_interval* inter)
{
  ASSERT(inter);
  darray_double_release(&inter->ka_nominal);
  darray_dbllst_release(&inter->ka_tab);
}

static INLINE res_T
layer_lw_spectral_interval_copy
  (struct layer_lw_spectral_interval* dst,
   const struct layer_lw_spectral_interval* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy(&dst->ka_nominal, &src->ka_nominal));
  CALL(darray_dbllst_copy(&dst->ka_tab, &src->ka_tab));
  #undef CALL
  return RES_OK;
}

static INLINE res_T
layer_lw_spectral_interval_copy_and_release
  (struct layer_lw_spectral_interval* dst,
   struct layer_lw_spectral_interval* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy_and_release(&dst->ka_nominal, &src->ka_nominal));
  CALL(darray_dbllst_copy_and_release(&dst->ka_tab, &src->ka_tab));
  #undef CALL
  layer_lw_spectral_interval_release(src);
  return RES_OK;
}

/* Generate the darray_lay_lw_specint dynamic array */
#define DARRAY_NAME lay_lw_specint
#define DARRAY_DATA struct layer_lw_spectral_interval
#define DARRAY_FUNCTOR_INIT layer_lw_spectral_interval_init
#define DARRAY_FUNCTOR_RELEASE layer_lw_spectral_interval_release
#define DARRAY_FUNCTOR_COPY layer_lw_spectral_interval_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE \
  layer_lw_spectral_interval_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Short wave spectral interval of a layer. Each data are defined per
 * quadrature point.
 ******************************************************************************/
struct layer_sw_spectral_interval {
  struct darray_double ka_nominal; /* Nominal absorption coef */
  struct darray_double ks_nominal; /* Nominal diffusion coef */
  struct darray_dbllst ka_tab; /* Tabulated absorption coef */
  struct darray_dbllst ks_tab; /* Tabulated diffusion coef */
};

static INLINE void
layer_sw_spectral_interval_init
  (struct mem_allocator* allocator, struct layer_sw_spectral_interval* inter)
{
  ASSERT(inter);
  darray_double_init(allocator, &inter->ka_nominal);
  darray_double_init(allocator, &inter->ks_nominal);
  darray_dbllst_init(allocator, &inter->ka_tab);
  darray_dbllst_init(allocator, &inter->ks_tab);
}

static INLINE void
layer_sw_spectral_interval_release(struct layer_sw_spectral_interval* inter)
{
  ASSERT(inter);
  darray_double_release(&inter->ka_nominal);
  darray_double_release(&inter->ks_nominal);
  darray_dbllst_release(&inter->ka_tab);
  darray_dbllst_release(&inter->ks_tab);
}

static INLINE res_T
layer_sw_spectral_interval_copy
  (struct layer_sw_spectral_interval* dst,
   const struct layer_sw_spectral_interval* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy(&dst->ka_nominal, &src->ka_nominal));
  CALL(darray_double_copy(&dst->ks_nominal, &src->ks_nominal));
  CALL(darray_dbllst_copy(&dst->ka_tab, &src->ka_tab));
  CALL(darray_dbllst_copy(&dst->ks_tab, &src->ks_tab));
  #undef CALL
  return RES_OK;
}

static INLINE res_T
layer_sw_spectral_interval_copy_and_release
  (struct layer_sw_spectral_interval* dst,
   struct layer_sw_spectral_interval* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy_and_release(&dst->ka_nominal, &src->ka_nominal));
  CALL(darray_double_copy_and_release(&dst->ks_nominal, &src->ks_nominal));
  CALL(darray_dbllst_copy_and_release(&dst->ka_tab, &src->ka_tab));
  CALL(darray_dbllst_copy_and_release(&dst->ks_tab, &src->ks_tab));
  #undef CALL
  layer_sw_spectral_interval_release(src);
  return RES_OK;
}

/* Generate the darray_lay_sw_specint dynamic array */
#define DARRAY_NAME lay_sw_specint
#define DARRAY_DATA struct layer_sw_spectral_interval
#define DARRAY_FUNCTOR_INIT layer_sw_spectral_interval_init
#define DARRAY_FUNCTOR_RELEASE layer_sw_spectral_interval_release
#define DARRAY_FUNCTOR_COPY layer_sw_spectral_interval_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE \
  layer_sw_spectral_interval_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Per layer data
 ******************************************************************************/
struct layer {
  double x_h2o_nominal; /* Nominal molar fraction of water vapor */
  struct darray_double x_h2o_tab; /* Tabulated xH2O */
  struct darray_lay_lw_specint lw_specints; /* Long wave spectral interval data */
  struct darray_lay_sw_specint sw_specints; /* Short wave spectral interval data */
};

static INLINE void
layer_init(struct mem_allocator* allocator, struct layer* layer)
{
  ASSERT(layer);
  darray_double_init(allocator, &layer->x_h2o_tab);
  darray_lay_lw_specint_init(allocator, &layer->lw_specints);
  darray_lay_sw_specint_init(allocator, &layer->sw_specints);
}

static INLINE void
layer_release(struct layer* layer)
{
  ASSERT(layer);
  darray_double_release(&layer->x_h2o_tab);
  darray_lay_lw_specint_release(&layer->lw_specints);
  darray_lay_sw_specint_release(&layer->sw_specints);
}

static INLINE res_T
layer_copy(struct layer* dst, const struct layer* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  dst->x_h2o_nominal = src->x_h2o_nominal;
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy(&dst->x_h2o_tab, &src->x_h2o_tab));
  CALL(darray_lay_lw_specint_copy(&dst->lw_specints, &src->lw_specints));
  CALL(darray_lay_sw_specint_copy(&dst->sw_specints, &src->sw_specints));
  #undef CALL
  return RES_OK;
}

static INLINE res_T
layer_copy_and_release(struct layer* dst, struct layer* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  dst->x_h2o_nominal = src->x_h2o_nominal;
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy_and_release(&dst->x_h2o_tab, &src->x_h2o_tab));
  CALL(darray_lay_lw_specint_copy_and_release(&dst->lw_specints, &src->lw_specints));
  CALL(darray_lay_sw_specint_copy_and_release(&dst->sw_specints, &src->sw_specints));
  #undef CALL
  layer_release(src);
  return RES_OK;
}

/* Generate the darray_layer dynamic array */
#define DARRAY_NAME layer
#define DARRAY_DATA struct layer
#define DARRAY_FUNCTOR_INIT layer_init
#define DARRAY_FUNCTOR_RELEASE layer_release
#define DARRAY_FUNCTOR_COPY layer_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE layer_copy_and_release
#include <rsys/dynamic_array.h>

#endif /* HTGOP_LAYER_H */

